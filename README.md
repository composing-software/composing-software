[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=compising-software)](https://sonarcloud.io/dashboard?id=compising-software)

# [Composing Software][1]

An Exploration of Functional Programming and Object Composition in Javascript.

[1]: https://www.amazon.com/-/es/Eric-Elliott/dp/1661212565
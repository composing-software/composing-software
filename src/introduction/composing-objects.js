class Foo {
  constructor () {
    this.a = 'a'
  }
}

class Bar extends Foo {
  constructor (options) {
    super(options)
    this.b = 'b'
  }
}
const a = { a: 'a' }
const b = { b: 'b' }
const c = { ...a, ...b }

module.exports = {
  Bar,
  c
}

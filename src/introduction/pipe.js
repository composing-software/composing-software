module.exports = (...functions) => x => functions.reduce((y, f) => f(y), x)
